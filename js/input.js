
function mostrarImagenSeleccionada() {
    const imagenInput = document.getElementById("imagenInput");
    const imagenMostrada = document.getElementById("imagenMostrada");

    imagenInput.addEventListener("change", function() {
        const archivo = imagenInput.files[0];

        if (archivo) {
            const reader = new FileReader();

            reader.onload = function(e) {
                imagenMostrada.src = e.target.result;
            };

            reader.readAsDataURL(archivo);
        } else {
            
            imagenMostrada.src = "0.png";
        }
    });
}


mostrarImagenSeleccionada();
