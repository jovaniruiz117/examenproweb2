
function mostrarTasasDeCambio() {
    fetch("/js/paises.json")
        .then(response => response.json())
        .then(data => {
            const infoTasasElement = document.getElementById("infoTasas");
            const posicionRange = document.getElementById("posicionRange");
            const posicionValor = document.getElementById("posicionValor");

            
            posicionRange.addEventListener("input", function() {
                const posicion = parseInt(this.value);
                posicionValor.textContent = posicion;
                const paisSeleccionado = data[posicion];

                const tablaHTML = "<table><tr><th>País</th><th>Moneda</th><th>Valor de Cambio</th></tr>" +
                    `<tr>
                        <td>${paisSeleccionado.pais}</td>
                        <td>${paisSeleccionado.moneda}</td>
                        <td>${paisSeleccionado.valor_cambio}</td>
                    </tr>` + "</table>";
                infoTasasElement.innerHTML = tablaHTML;
            });

           
            const primerPais = data[0];
            const tablaHTML = "<table><tr><th>País</th><th>Moneda</th><th>Valor de Cambio</th></tr>" +
                `<tr>
                    <td>${primerPais.pais}</td>
                    <td>${primerPais.moneda}</td>
                    <td>${primerPais.valor_cambio}</td>
                </tr>` + "</table>";
            infoTasasElement.innerHTML = tablaHTML;
        })
        .catch(error => {
            console.error("Error al cargar el archivo JSON: " + error);
        });
}


mostrarTasasDeCambio();
